import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
from django.core.mail import send_mail
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        # ALL OF YOUR CODE THAT HANDLES READING FROM THE
        # QUEUES AND SENDING EMAILS
        def presentation_approval(ch, method, properties, body):
            presentation = json.loads(body)
            name = presentation["presenter_name"]
            title = presentation["title"]
            send_mail(
                "Your presentation has been accepted",
                f"{name}, we're happy to tell you that your presentation {title} has been accepted",
                "admin@conference.go",
                [presentation["presenter_email"]],
                fail_silently=False,
            )

        def presentation_rejection(ch, method, properties, body):   
            presentation = json.loads(body)
            name = presentation["presenter_name"]
            title = presentation["title"]
            send_mail(
                "Your presentation has been rejected",
                f"{name}, we're sorry to tell you that your presentation {title} has been rejected",
                "admin@conference.go",
                [presentation["presenter_email"]],
                fail_silently=False,
            )    

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()  
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=presentation_approval,
            auto_ack=True,
        )

        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=presentation_rejection,
            auto_ack=True,
        )

        channel.start_consuming()  
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)









